# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fde-monc <fde-monc@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/04/14 20:22:06 by fde-monc          #+#    #+#              #
#    Updated: 2016/04/15 14:55:15 by fde-monc         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

CC = gcc

CFLAGS = -g -Wall -Werror -Wextra

PATH_SRC = src/

SRC =	ls_compare.c \
		ls_getopt.c \
		ls_loption.c \
		ls_loption2.c \
		ls_main.c \
		ls_makelist.c \
		ls_opendir.c \
		ls_printl.c \
		ls_prints.c

SRC_FILE = $(addprefix $(PATH_SRC), $(SRC));

OBJECTS = $(patsubst %.c,%.o,$(addprefix $(PATH_SRC), $(SRC)))

LIB = libft

LIB_EXE = libft/libft.a

all : $(NAME)

$(NAME) : $(OBJECTS) $(LIB_EXE)
	 $(CC) $(CFLAGS) -Iinc/ -L$(LIB) -lft $(OBJECTS) -o $(NAME)

$(LIB_EXE) :
	make -C $(LIB) nclean

$(OBJECTS) : %.o : %.c
	$(CC) $(CFLAGS) -Iinc/ -c $< -o $@

clean : all
	@rm -rf $(OBJECTS)

fclean : clean
	@rm -rf $(NAME)

re : fclean $(NAME)

nclean : all clean
