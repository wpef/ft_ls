/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_main.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 14:35:22 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/05 17:17:53 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		main(int ac, char **av)
{
	if (ac >= 1)
	{
		if (ls_getopt(av) > 0)
			ls_print();
	}
	return (0);
}

void	ls_print(void)
{
	if (ls_printfiles() == 1)
	{
		if (g_ls.path == NULL)
			return ;
	}
	ls_checkpaths();
}
