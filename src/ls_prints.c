/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_prints.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <fde-monc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/05 14:48:03 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/05 17:50:22 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int	ls_stdprint(void)
{
	t_lslist	*curs;

	curs = g_ls.files;
	if (curs == NULL)
		return (-1);
	while (curs != NULL)
	{
		ft_putendl(curs->content);
		curs = curs->next;
	}
	if (g_ls.f_rr == 0 && g_ls.pathcount > 1)
	{
		if (g_ls.path && g_ls.path->next != NULL)
			ft_putchar('\n');
	}
	return (1);
}

int	ls_isfirst(char *path)
{
	int	ret;

	if (!path || !(g_ls.firstpath))
		return (-1);
	ret = ft_strcmp(path, g_ls.firstpath);
	if (ret == 0)
		return (1);
	return (0);
}
