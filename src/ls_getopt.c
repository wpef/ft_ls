/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_getopt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <fde-monc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 18:46:41 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/15 15:07:31 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		ls_strucinit(void)
{
	g_ls.f_l = 0;
	g_ls.f_rr = 0;
	g_ls.f_a = 0;
	g_ls.f_r = 0;
	g_ls.f_t = 0;
	g_ls.pathcount = 0;
	g_ls.firstpath = NULL;
	g_ls.path = NULL;
	g_ls.blocks = 0;
	g_ls.files = NULL;
}

int			ls_getopt(char **av)
{
	int	s;

	ls_strucinit();
	s = 1;
	while (av[s] && av[s][0] == '-' && av[s][1] != '\0')
	{
		if (ft_strcmp(av[s], "--") == 0)
		{
			s++;
			break ;
		}
		if (ls_getflags(av[s]) == 0)
			return (-1);
		s++;
	}
	ls_cmpinit();
	while (av[s])
	{
		if (ls_checkarg(av[s]) == -1 && (g_ls.pathcount == 1 && !av[s + 1]))
			exit(EXIT_FAILURE);
		s++;
	}
	return (1);
}

int			ls_getflags(char *arg)
{
	int	i;

	i = 1;
	while (arg[i])
	{
		if (arg[i] == 'l')
			g_ls.f_l = 1;
		else if (arg[i] == 'R')
			g_ls.f_rr = 1;
		else if (arg[i] == 'a')
			g_ls.f_a = 1;
		else if (arg[i] == 'r')
			g_ls.f_r = 1;
		else if (arg[i] == 't')
			g_ls.f_t = 1;
		else
		{
			ft_cdebug("ft_ls: illegal option -- %", arg[i]);
			ft_putendl("usage: ./ls [-Ralrt] [file ...]");
			return (0);
		}
		i++;
	}
	return (1);
}

int			ls_checkarg(char *arg)
{
	t_stat	*buf;

	buf = malloc(sizeof(struct stat));
	g_ls.pathcount++;
	if ((lstat(arg, buf) == -1))
	{
		if (arg[0] == '\0')
		{
			ft_putendl("ls: fts_open: No such file or directory");
			exit(EXIT_SUCCESS);
		}
		ft_sdebug("ls: %: No such file or directory", arg);
		free(buf);
		return (-1);
	}
	return (ls_sendtolist(buf, arg));
}

int			ls_sendtolist(t_stat *buf, char *arg)
{
	t_stat	*buf2;

	if (S_ISREG(buf->st_mode) || (S_ISLNK(buf->st_mode) && g_ls.f_l == 1))
		ls_makelist(arg, NULL, &g_ls.files);
	else if (S_ISDIR(buf->st_mode))
		ls_makelist(arg, NULL, &g_ls.path);
	else
	{
		buf2 = malloc(sizeof(struct stat));
		stat(arg, buf2);
		ls_makelist(arg, NULL, ((S_ISDIR(buf2->st_mode)) ?
					&g_ls.path : &g_ls.files));
		free(buf2);
	}
	free(buf);
	return (1);
}
