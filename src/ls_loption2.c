/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_loption2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <fde-monc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 17:13:48 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/05 17:17:22 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ls_getdevid(dev_t rdev)
{
	unsigned int	minor;
	unsigned int	major;

	minor = minor(rdev);
	major = major(rdev);
	if (major < 10)
		ft_putchar(' ');
	if (major < 100)
		ft_putchar(' ');
	if (major < 1000)
		ft_putchar(' ');
	ft_putnbr(major);
	ft_putstr(", ");
	if (minor < 10)
		ft_putchar(' ');
	if (minor < 100)
		ft_putchar(' ');
	if (minor < 1000)
		ft_putchar(' ');
	ft_putnbr(minor);
}

void	ls_afflink(char *path)
{
	char buf[1024];

	ft_bzero(buf, 1024);
	if (readlink(path, buf, 1024) >= 0)
	{
		ft_putstr(" -> ");
		ft_putendl(buf);
	}
}

void	ls_getsize(off_t st_size)
{
	int size;

	size = (int)st_size;
	ls_affesp(size);
	ft_putnbr(size);
}

void	ls_gettime(time_t mtime)
{
	char *time;
	char *tmp;

	time = ft_strdup(ctime(&mtime));
	tmp = ft_strsub(time, 4, 12);
	ft_putstr(tmp);
	free(tmp);
	ft_putchar(' ');
}

void	ls_affesp(int size)
{
	ft_putchar(' ');
	if (size < 10)
		ft_putchar(' ');
	if (size < 100)
		ft_putchar(' ');
	if (size < 1000)
		ft_putchar(' ');
	if (size < 10000)
		ft_putchar(' ');
	if (size < 100000)
		ft_putchar(' ');
	if (size < 1000000)
		ft_putchar(' ');
}
