/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_compare.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 21:55:29 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/05 17:14:46 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** FREES !!!
*/

int		cmp_alpha(t_lslist *list, t_lslist *maillon)
{
	return (ft_strcmp(list->content, maillon->content));
}

int		cmp_alpha_rev(t_lslist *list, t_lslist *maillon)
{
	return (-(ft_strcmp(list->content, maillon->content)));
}

int		cmp_time(t_lslist *list, t_lslist *maillon)
{
	if ((maillon->stat.st_mtime) < (list->stat.st_mtime))
		return (maillon->stat.st_mtime - list->stat.st_mtime);
	else if (maillon->stat.st_mtime == list->stat.st_mtime)
	{
		if (maillon->stat.st_mtimespec.tv_nsec
			< list->stat.st_mtimespec.tv_nsec)
			return (maillon->stat.st_mtimespec.tv_nsec
				- list->stat.st_mtimespec.tv_nsec);
		else if (maillon->stat.st_mtimespec.tv_nsec
			== list->stat.st_mtimespec.tv_nsec)
			return (cmp_alpha(list, maillon));
	}
	return (1);
}

int		cmp_time_rev(t_lslist *list, t_lslist *maillon)
{
	if ((list->stat.st_mtime) < (maillon->stat.st_mtime))
		return (list->stat.st_mtime - maillon->stat.st_mtime);
	else if (list->stat.st_mtime == maillon->stat.st_mtime)
	{
		if (list->stat.st_mtimespec.tv_nsec
			< maillon->stat.st_mtimespec.tv_nsec)
			return (list->stat.st_mtimespec.tv_nsec
				- maillon->stat.st_mtimespec.tv_nsec);
		else if (list->stat.st_mtimespec.tv_nsec
			== maillon->stat.st_mtimespec.tv_nsec)
			return (cmp_alpha(maillon, list));
	}
	return (1);
}

void	ls_cmpinit(void)
{
	if (g_ls.f_r == 1)
	{
		if (g_ls.f_t == 0)
			g_ls.cmp = cmp_alpha_rev;
		else if (g_ls.f_t == 1)
			g_ls.cmp = cmp_time_rev;
	}
	else
	{
		if (g_ls.f_t == 1)
			g_ls.cmp = cmp_time;
		else
			g_ls.cmp = cmp_alpha;
	}
}
