/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_loption.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 19:15:23 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/05 17:16:40 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ls_gettype(mode_t st_mode)
{
	if (S_ISDIR(st_mode))
		ft_putchar('d');
	else if (S_ISLNK(st_mode))
		ft_putchar('l');
	else if (S_ISBLK(st_mode))
		ft_putchar('b');
	else if (S_ISCHR(st_mode))
		ft_putchar('c');
	else if (S_ISFIFO(st_mode))
		ft_putchar('p');
	else
		ft_putchar('-');
}

void	ls_getrights(mode_t st_mode)
{
	ft_putchar((st_mode & S_IRUSR) ? 'r' : '-');
	ft_putchar((st_mode & S_IWUSR) ? 'w' : '-');
	if (st_mode & S_IXUSR)
		ft_putchar((st_mode & S_ISUID) ? 's' : 'x');
	else
		ft_putchar('-');
	ft_putchar((st_mode & S_IRGRP) ? 'r' : '-');
	ft_putchar((st_mode & S_IWGRP) ? 'w' : '-');
	if (st_mode & S_IXGRP)
		ft_putchar((st_mode & S_ISGID) ? 's' : 'x');
	else
		ft_putchar('-');
	ft_putchar((st_mode & S_IROTH) ? 'r' : '-');
	ft_putchar((st_mode & S_IWOTH) ? 'w' : '-');
	if (st_mode & S_ISVTX)
		ft_putchar((st_mode & S_IXOTH) ? 't' : 'T');
	else
		ft_putchar((st_mode & S_IXOTH) ? 'x' : '-');
}

void	ls_gethardlinks(nlink_t st_nlink)
{
	int	link;

	link = (int)st_nlink;
	ls_affesp(link);
	ft_putnbr(link);
}

void	ls_getusrname(uid_t uid)
{
	struct passwd *pws;

	pws = getpwuid(uid);
	if (pws)
		ft_putstr(pws->pw_name);
	else
	{
		ft_putnbr(uid);
		ft_putchar('\t');
	}
	ft_putchar('\t');
	ft_putstr("  ");
}

void	ls_getgrpname(gid_t gid)
{
	struct group *grp;

	grp = getgrgid(gid);
	if (grp)
		ft_putstr(grp->gr_name);
	else
	{
		ft_putnbr(gid);
		ft_putchar('\t');
	}
	ft_putchar('\t');
	ft_putchar(' ');
}
