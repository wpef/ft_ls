/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_makelist.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <fde-monc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 17:14:27 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/05 17:19:35 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		ls_makelist(char *arg, char *path, t_lslist **list)
{
	char		*filepath;

	filepath = ft_makepath(path, arg);
	if (*list == NULL)
	{
		*list = ls_makenode(arg, filepath);
		return ;
	}
	ls_sortlist(arg, filepath, list);
	return ;
}

t_lslist	*ls_makenode(char *arg, char *filepath)
{
	t_lslist	*ptr;

	ptr = malloc(sizeof(t_lslist));
	ptr->content = ft_strdup(arg);
	lstat(filepath, &ptr->stat);
	ptr->path = ft_strdup(filepath);
	g_ls.blocks = g_ls.blocks + ptr->stat.st_blocks;
	ptr->next = NULL;
	return (ptr);
}

void		ls_sortlist(char *arg, char *filepath, t_lslist **list)
{
	t_lslist	*ptr;
	t_lslist	*curs;

	curs = *list;
	ptr = ls_makenode(arg, filepath);
	if (g_ls.cmp(curs, ptr) > 0)
	{
		ptr->next = curs;
		*list = ptr;
		return ;
	}
	while (curs->next != NULL)
	{
		if (g_ls.cmp(curs->next, ptr) > 0)
		{
			ptr->next = curs->next;
			curs->next = ptr;
			return ;
		}
		curs = curs->next;
	}
	curs->next = ptr;
}

char		*ft_makepath(char *path, char *arg)
{
	char	*tmp;
	char	*tmp2;

	if (path == NULL)
		return (arg);
	else
	{
		tmp = ft_strjoin(path, "/");
		tmp2 = ft_strjoin(tmp, arg);
		free(tmp);
	}
	return (tmp2);
}
