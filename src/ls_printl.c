/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_printl.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <fde-monc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 18:22:24 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/14 20:15:26 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ls_printfiles_l(void)
{
	t_lslist	*curs;

	curs = g_ls.files;
	if (curs && S_ISLNK(curs->stat.st_mode))
		;
	else if (g_ls.path && S_ISDIR(g_ls.path->stat.st_mode))
		ft_idebug("total %", g_ls.blocks);
	while (curs != NULL)
	{
		ls_gettype(curs->stat.st_mode);
		ls_getrights(curs->stat.st_mode);
		ft_putchar(' ');
		ls_gethardlinks(curs->stat.st_nlink);
		ft_putchar(' ');
		ls_getusrname(curs->stat.st_uid);
		ls_getgrpname(curs->stat.st_gid);
		ls_printfiles_l_next(curs);
		curs = curs->next;
	}
}

void	ls_printfiles_l_next(t_lslist *curs)
{
	if (S_ISCHR(curs->stat.st_mode) || S_ISBLK(curs->stat.st_mode))
		ls_getdevid(curs->stat.st_rdev);
	else
		ls_getsize(curs->stat.st_size);
	ft_putstr("  ");
	ls_gettime(curs->stat.st_mtime);
	if (S_ISLNK(curs->stat.st_mode))
	{
		ft_putstr(curs->content);
		ls_afflink(curs->path);
	}
	else
		ft_putendl(curs->content);
}
