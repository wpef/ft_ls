/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_opendir.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 15:45:06 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/15 15:16:28 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			ls_checkpaths(void)
{
	if (g_ls.path == NULL && g_ls.pathcount == 0)
	{
		g_ls.path = malloc(sizeof(t_lslist));
		g_ls.path->content = ft_strdup(".");
		lstat(".", &g_ls.path->stat);
		g_ls.path->next = NULL;
	}
	g_ls.firstpath = g_ls.path ? ft_strdup(g_ls.path->content) : NULL;
	while (g_ls.path != NULL)
	{
		ls_opendir(g_ls.path->content, g_ls.path->content);
		g_ls.path = g_ls.path->next;
	}
}

void			ls_opendir(char *path, char *file)
{
	DIR	*dir;

	g_ls.files = NULL;
	g_ls.blocks = 0;
	if (g_ls.f_rr == 0 && g_ls.pathcount > 1)
	{
		ft_putstr(path);
		ft_putendl(":");
	}
	else if (g_ls.f_rr == 1)
	{
		if (ls_isfirst(path) != 1)
			ft_putchar('\n');
		ft_sdebug("%:", path);
	}
	if ((dir = opendir(path)) != NULL)
		ls_readdir(dir, path);
	else
		ft_sdebug("ls: %: Permission denied", file);
}

void			ls_readdir(DIR *dir, char *path)
{
	struct dirent	*struc;

	if (g_ls.f_a == 1)
		while ((struc = readdir(dir)) != NULL)
			ls_makelist(struc->d_name, path, &g_ls.files);
	else
		while ((struc = readdir(dir)) != NULL)
			if (struc->d_name[0] != '.')
				ls_makelist(struc->d_name, path, &g_ls.files);
	ls_printfiles();
	if (g_ls.f_rr == 1)
		ls_recurse();
	closedir(dir);
}

int				ls_printfiles(void)
{
	if (g_ls.files != NULL && g_ls.f_l == 0)
		return (ls_stdprint());
	else if (g_ls.files != NULL && g_ls.f_l == 1)
	{
		ls_printfiles_l();
		if (g_ls.path && g_ls.path->next != NULL && g_ls.f_rr == 0)
			ft_putchar('\n');
		return (1);
	}
	return (0);
}

void			ls_recurse(void)
{
	t_lslist	*curs;

	curs = g_ls.files;
	while (curs != NULL)
	{
		if (S_ISDIR(curs->stat.st_mode)
			&& ft_strcmp(curs->content, ".") != 0
			&& ft_strcmp(curs->content, "..") != 0)
			ls_opendir(curs->path, curs->content);
		curs = curs->next;
	}
}
