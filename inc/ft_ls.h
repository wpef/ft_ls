/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fde-monc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 19:13:32 by fde-monc          #+#    #+#             */
/*   Updated: 2016/04/05 17:12:48 by fde-monc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H
# include "libft.h"
# include <dirent.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <uuid/uuid.h>
# include <time.h>

typedef struct stat	t_stat;

typedef struct		s_lslist
{
	char			*content;
	char			*path;
	t_stat			stat;
	struct s_lslist	*next;
}					t_lslist;

typedef struct		s_ls
{
	char			f_l;
	char			f_rr;
	char			f_a;
	char			f_r;
	char			f_t;
	int				pathcount;
	char			*firstpath;
	unsigned long	blocks;
	int				(*cmp)(t_lslist *list, t_lslist *maillon);
	t_lslist		*path;
	t_lslist		*files;
}					t_ls;

void				ls_print(void);
void				ls_strucinit(void);
int					ls_getopt(char **av);
int					ls_checkarg(char *arg);
int					ls_getflags (char *arg);
int					ls_sendtolist(t_stat *buf, char *arg);
int					ls_printfiles(void);

void				ls_checkpaths(void);
void				ls_opendir(char *path, char *file);
void				ls_readdir(DIR *dir, char *path);
void				ls_recurse(void);

void				ls_makelist(char *arg, char *path, t_lslist **curs);
t_lslist			*ls_makenode(char *arg, char *filepath);
void				ls_sortlist(char *arg, char *filepath, t_lslist **list);
char				*ft_makepath(char *path, char *file);

int					cmp_alpha(t_lslist *list, t_lslist *maillon);
int					cmp_alpha_rev(t_lslist *list, t_lslist *maillon);
int					cmp_time(t_lslist *list, t_lslist *maillon);
void				ls_cmpinit(void);

void				ls_affblocks(void);
void				ls_gettype(mode_t st_mode);
void				ls_getrights(mode_t st_mode);
void				ls_gethardlinks(nlink_t st_nlink);
void				ls_getusrname(uid_t uid);
void				ls_getgrpname(gid_t gid);
void				ls_getsize(off_t st_size);
void				ls_getdevid(dev_t rdev);
void				ls_gettime(time_t mtime);
void				ls_affesp(int size);
void				ls_afflink(char *path);

void				ls_printfiles_l(void);
void				ls_printfiles_l_next(t_lslist *curs);
void				ls_get_linklength(size_t length);
int					ls_stdprint(void);
int					ls_isfirst(char *path);

t_ls				g_ls;

#endif
